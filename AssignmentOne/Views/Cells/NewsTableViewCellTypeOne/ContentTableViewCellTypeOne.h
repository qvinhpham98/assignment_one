//
//  ContentTableViewCellTypeOne.h
//  AssignmentOne
//
//  Created by LAP12230 on 2/14/20.
//  Copyright © 2020 LAP12230. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "ContentTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface ContentTableViewCellTypeOne : ContentTableViewCell

@property (nonatomic, strong) UIImageView *avatarImageView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *publisherLabel;
@property (nonatomic, strong) UILabel *separatorLabel;
@property (nonatomic, strong) UILabel *timeLabel;

 @end

NS_ASSUME_NONNULL_END
