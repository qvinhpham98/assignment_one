//
//  ContentTableViewCellTypeOne.m
//  AssignmentOne
//
//  Created by LAP12230 on 2/14/20.
//  Copyright © 2020 LAP12230. All rights reserved.
//

#import "ContentTableViewCellTypeOne.h"
#import "Constants.h"
#import "Utility.h"

#import <SDWebImage/SDWebImage.h>

@implementation ContentTableViewCellTypeOne


- (void)setupSubviews {
    
    // Avatar
    self.avatarImageView = [[UIImageView alloc] init];
    [self.contentView addSubview:self.avatarImageView];
    self.avatarImageView.backgroundColor = UIColor.orangeColor;
    self.avatarImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.avatarImageView.clipsToBounds = true;
    self.avatarImageView.layer.cornerRadius = CONTENT_IMAGE_BORDER_RADIUS;
    
    // Title label
    self.titleLabel = [[UILabel alloc] init];
    self.titleLabel.numberOfLines = 3;
    self.titleLabel.font = [UIFont systemFontOfSize:CONTENT_TITLE_SIZE];
    [self.contentView addSubview:self.titleLabel];
    
    // Publisher label
    self.publisherLabel = [[UILabel alloc] init];
    self.publisherLabel.font = [UIFont systemFontOfSize:12];
    self.publisherLabel.textColor = UIColor.lightGrayColor;
    [self.contentView addSubview:self.publisherLabel];
    
    // Time label
    self.timeLabel = [[UILabel alloc] init];
    self.timeLabel.font = [UIFont systemFontOfSize:12];
    self.timeLabel.textColor = UIColor.lightGrayColor;
    [self.contentView addSubview:self.timeLabel];
    
    // Time label
    self.separatorLabel = [[UILabel alloc] init];
    self.separatorLabel.font = [UIFont systemFontOfSize:12];
    self.separatorLabel.textColor = UIColor.lightGrayColor;
    self.separatorLabel.text = @"・";
    [self.contentView addSubview:self.separatorLabel];
}


- (void)setupConstraints {
    self.avatarImageView.translatesAutoresizingMaskIntoConstraints = false;
    self.titleLabel.translatesAutoresizingMaskIntoConstraints = false;
    self.publisherLabel.translatesAutoresizingMaskIntoConstraints = false;
    self.timeLabel.translatesAutoresizingMaskIntoConstraints = false;
    self.separatorLabel.translatesAutoresizingMaskIntoConstraints = false;
    
    [NSLayoutConstraint activateConstraints:
     @[
       // Avatar
       [self.avatarImageView.leadingAnchor constraintEqualToAnchor:self.avatarImageView.superview.leadingAnchor constant:CONTENT_PADDING],
       [self.avatarImageView.topAnchor constraintEqualToAnchor:self.avatarImageView.superview.topAnchor constant:CONTENT_PADDING],
       [self.avatarImageView.bottomAnchor constraintEqualToAnchor:self.avatarImageView.superview.bottomAnchor constant:-CONTENT_PADDING],
       [self.avatarImageView.heightAnchor constraintEqualToConstant:CONTENT_IMAGE_HEIGHT],
       [self.avatarImageView.widthAnchor constraintEqualToAnchor:self.avatarImageView.heightAnchor multiplier:1.3 constant:0],
       
       // Title label
       [self.titleLabel.topAnchor constraintEqualToAnchor:self.avatarImageView.topAnchor],
       [self.titleLabel.leftAnchor constraintEqualToAnchor:self.avatarImageView.rightAnchor constant:20],
       [self.titleLabel.rightAnchor constraintEqualToAnchor:self.titleLabel.superview.rightAnchor constant:-CONTENT_PADDING],
       
       // Publisher label
       [self.publisherLabel.bottomAnchor constraintEqualToAnchor:self.avatarImageView.bottomAnchor],
       [self.publisherLabel.leftAnchor constraintEqualToAnchor:self.avatarImageView.rightAnchor constant: 20],
       
       // Separator label
       [self.separatorLabel.leftAnchor constraintEqualToAnchor:self.publisherLabel.rightAnchor],
       [self.separatorLabel.bottomAnchor constraintEqualToAnchor:self.publisherLabel.bottomAnchor],
       
       // Time label
       [self.timeLabel.bottomAnchor constraintEqualToAnchor:self.avatarImageView.bottomAnchor],
       [self.timeLabel.leftAnchor constraintEqualToAnchor:self.separatorLabel.rightAnchor],
       ]];
}


- (void)configureCellWith:(Content *)content {
    [self.avatarImageView sd_setImageWithURL:content.avatarUrl];
    self.titleLabel.text = content.title;
    self.publisherLabel.text = content.publisherName;
    
    NSDateComponents *dateComponent = [Utility dateComponentsFromDate:content.date toDate:[NSDate date]];
    self.timeLabel.text = [ContentTableViewCell timeStringFormatted:dateComponent];
}


@end
