//
//  ContentTableViewCellTypeTwo.m
//  AssignmentOne
//
//  Created by LAP12230 on 2/14/20.
//  Copyright © 2020 LAP12230. All rights reserved.
//

#import "ContentTableViewCellTypeTwo.h"
#import "Constants.h"
#import "Utility.h"

#import <SDWebImage/SDWebImage.h>

@implementation ContentTableViewCellTypeTwo


- (void)setupSubviews{
    
    // Title
    self.titleLabel = [[UILabel alloc] init];
    self.titleLabel.font = [UIFont systemFontOfSize:CONTENT_TITLE_SIZE];
    
    self.titleLabel.numberOfLines = 3;
    [self.contentView addSubview:self.titleLabel];
    
    // Images StackView
    self.imagesStackView = [[UIStackView alloc] init];
    self.imagesStackView.axis = UILayoutConstraintAxisHorizontal;
    self.imagesStackView.distribution = UIStackViewDistributionFillEqually;
    self.imagesStackView.alignment = UIStackViewAlignmentFill;
    self.imagesStackView.spacing = 5;
    for (NSInteger i = 0; i < 3; i++) {
        UIImageView *imageView = [[UIImageView alloc] init];
        imageView.backgroundColor = UIColor.orangeColor;
        imageView.layer.cornerRadius = CONTENT_IMAGE_BORDER_RADIUS;
        imageView.contentMode = UIViewContentModeScaleAspectFill;
        imageView.clipsToBounds = true;
        [self.imagesStackView addArrangedSubview:imageView];
    }
    [self.contentView addSubview:self.imagesStackView];
    
    // Publisher
    self.publisherLabel = [[UILabel alloc] init];
    self.publisherLabel.font = [UIFont systemFontOfSize:12];
    self.publisherLabel.textColor = UIColor.lightGrayColor;
    [self.contentView addSubview:self.publisherLabel];
    
    // Time
    self.timeLabel = [[UILabel alloc] init];
    self.timeLabel.font = [UIFont systemFontOfSize:12];
    self.timeLabel.textColor = UIColor.lightGrayColor;
    [self.contentView addSubview:self.timeLabel];
    
    // Separator Text
    self.separatorLabel = [[UILabel alloc] init];
    self.separatorLabel.font = [UIFont systemFontOfSize:12];
    self.separatorLabel.textColor = UIColor.lightGrayColor;
    self.separatorLabel.text = @"・";
    [self.contentView addSubview:self.separatorLabel];
    
}


- (void)setupConstraints {
    self.titleLabel.translatesAutoresizingMaskIntoConstraints = false;
    self.timeLabel.translatesAutoresizingMaskIntoConstraints = false;
    self.publisherLabel.translatesAutoresizingMaskIntoConstraints = false;
    self.imagesStackView.translatesAutoresizingMaskIntoConstraints = false;
    self.separatorLabel.translatesAutoresizingMaskIntoConstraints = false;
    
    [NSLayoutConstraint activateConstraints:
     @[
       // Title
       [self.titleLabel.topAnchor constraintEqualToAnchor:self.contentView.topAnchor constant:CONTENT_PADDING],
       [self.titleLabel.leftAnchor constraintEqualToAnchor:self.contentView.leftAnchor constant:CONTENT_PADDING],
       [self.titleLabel.rightAnchor constraintEqualToAnchor:self.contentView.rightAnchor constant:-CONTENT_PADDING],
       
       // StackView
       [self.imagesStackView.topAnchor constraintEqualToAnchor:self.titleLabel.bottomAnchor constant:20],
       [self.imagesStackView.leftAnchor constraintEqualToAnchor:self.titleLabel.leftAnchor],
       [self.imagesStackView.rightAnchor constraintEqualToAnchor:self.titleLabel.rightAnchor],
       [self.imagesStackView.heightAnchor constraintEqualToConstant:CONTENT_IMAGE_HEIGHT],
       
       // Publisher
       [self.publisherLabel.leftAnchor constraintEqualToAnchor:self.titleLabel.leftAnchor],
       [self.publisherLabel.topAnchor constraintEqualToAnchor:self.imagesStackView.bottomAnchor constant:20],
       [self.publisherLabel.bottomAnchor constraintEqualToAnchor:self.contentView.bottomAnchor constant:-CONTENT_PADDING],
       
       // Separator Text
       [self.separatorLabel.leftAnchor constraintEqualToAnchor:self.publisherLabel.rightAnchor],
       [self.separatorLabel.bottomAnchor constraintEqualToAnchor:self.publisherLabel.bottomAnchor],
       
       // Time Label
       [self.timeLabel.leftAnchor constraintEqualToAnchor:self.separatorLabel.rightAnchor],
       [self.timeLabel.bottomAnchor constraintEqualToAnchor:self.publisherLabel.bottomAnchor],
       ]];
}


- (void)configureCellWith:(Content *)content {
    self.titleLabel.text = content.title;
    self.publisherLabel.text = content.publisherName;
    self.timeLabel.text = [NSString stringWithFormat: @"%ld", (long)content.date];
    
    NSDateComponents *dateComponent = [Utility dateComponentsFromDate:content.date toDate:[NSDate date]];
    self.timeLabel.text = [ContentTableViewCell timeStringFormatted:dateComponent];
    
    for (int i = 0;
         i < content.images.count &&
         i < self.imagesStackView.arrangedSubviews.count;
         i++) {
        UIImageView *imageView = (UIImageView *)self.imagesStackView.arrangedSubviews[i];
        if (imageView) {
            [imageView sd_setImageWithURL:content.images[i].url];
        }
    }
}


@end
