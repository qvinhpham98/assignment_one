//
//  ContentTableViewCellTypeTwo.h
//  AssignmentOne
//
//  Created by LAP12230 on 2/14/20.
//  Copyright © 2020 LAP12230. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ContentTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface ContentTableViewCellTypeTwo : ContentTableViewCell

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *publisherLabel;
@property (nonatomic, strong) UILabel *timeLabel;
@property (nonatomic, strong) UILabel *separatorLabel;
@property (nonatomic, strong) UIStackView *imagesStackView;
@property (nonatomic, strong) NSArray<UIImageView *> *imageViews;

@end

NS_ASSUME_NONNULL_END
