//
//  NewsTableViewCellTypeThree.h
//  AssignmentOne
//
//  Created by LAP12230 on 2/17/20.
//  Copyright © 2020 LAP12230. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "ContentTableViewCell.h"
#import "Constants.h"

NS_ASSUME_NONNULL_BEGIN

@interface ContentTableViewCellTypeThree : ContentTableViewCell

@property (nonatomic, strong) UIView *container;
@property (nonatomic, strong) UIImageView *contentImageView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *publisherLabel;
@property (nonatomic, strong) UILabel *timeLabel;
@property (nonatomic, strong) UILabel *separatorLabel;

@end

NS_ASSUME_NONNULL_END
