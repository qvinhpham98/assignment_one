//
//  NewsTableViewCellTypeThree.m
//  AssignmentOne
//
//  Created by LAP12230 on 2/17/20.
//  Copyright © 2020 LAP12230. All rights reserved.
//

#import "ContentTableViewCellTypeThree.h"
#import <SDWebImage/SDWebImage.h>
#import "Utility.h"

@implementation ContentTableViewCellTypeThree


- (void)setupSubviews {
    [super setupSubviews];
    
    // Container
    self.container = [[UIView alloc] init];
    self.container.backgroundColor = UIColor.whiteColor;
    self.container.layer.cornerRadius = 5.0f;
    self.container.layer.masksToBounds = true;
    self.container.clipsToBounds = true;
    [self.contentView addSubview:self.container];
    
    // Content Image
    self.contentImageView = [[UIImageView alloc] init];
    self.contentImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.contentImageView.clipsToBounds = true;
    [self.container addSubview:self.contentImageView];
    
    // Title
    self.titleLabel = [[UILabel alloc] init];
    self.titleLabel.font = [UIFont systemFontOfSize:(CONTENT_TITLE_SIZE + 2) weight:UIFontWeightSemibold];
    self.titleLabel.numberOfLines = 3;
    [self.container addSubview:self.titleLabel];
    
    // Publisher
    self.publisherLabel = [[UILabel alloc] init];
    self.publisherLabel.font = [UIFont systemFontOfSize:12];
    self.publisherLabel.textColor = UIColor.lightGrayColor;
    [self.container addSubview:self.publisherLabel];
    
    // Time
    self.timeLabel = [[UILabel alloc] init];
    self.timeLabel.font = [UIFont systemFontOfSize:12];
    self.timeLabel.textColor = UIColor.lightGrayColor;
    [self.container addSubview:self.timeLabel];
    
    // Separator Text
    self.separatorLabel = [[UILabel alloc] init];
    self.separatorLabel.font = [UIFont systemFontOfSize:12];
    self.separatorLabel.textColor = UIColor.lightGrayColor;
    self.separatorLabel.text = @"・";
    [self.container addSubview:self.separatorLabel];
}


- (void)setupConstraints {
    [super setupConstraints];
    self.container.translatesAutoresizingMaskIntoConstraints = false;
    self.contentImageView.translatesAutoresizingMaskIntoConstraints = false;
    self.titleLabel.translatesAutoresizingMaskIntoConstraints = false;
    self.separatorLabel.translatesAutoresizingMaskIntoConstraints = false;
    self.publisherLabel.translatesAutoresizingMaskIntoConstraints = false;
    self.timeLabel.translatesAutoresizingMaskIntoConstraints = false;
    
    [NSLayoutConstraint activateConstraints:
     @[
       // Container View
       [self.container.topAnchor constraintEqualToAnchor:self.contentView.topAnchor constant:CONTENT_PADDING],
       [self.container.leadingAnchor constraintEqualToAnchor:self.contentView.leadingAnchor constant:CONTENT_PADDING],
       [self.container.trailingAnchor constraintEqualToAnchor:self.contentView.trailingAnchor constant:-CONTENT_PADDING],
       [self.container.bottomAnchor constraintEqualToAnchor:self.contentView.bottomAnchor constant:-CONTENT_PADDING],
       
       // Content Image View
       [self.contentImageView.topAnchor constraintEqualToAnchor:self.container.topAnchor],
       [self.contentImageView.leadingAnchor constraintEqualToAnchor:self.container.leadingAnchor],
       [self.contentImageView.trailingAnchor constraintEqualToAnchor:self.container.trailingAnchor],
       [self.contentImageView.heightAnchor constraintEqualToAnchor:self.contentImageView.widthAnchor multiplier:0.60f],
       
       // Title Label
       [self.titleLabel.topAnchor constraintEqualToAnchor:self.contentImageView.bottomAnchor constant:10],
       [self.titleLabel.leadingAnchor constraintEqualToAnchor:self.contentImageView.leadingAnchor constant:10],
       [self.titleLabel.trailingAnchor constraintEqualToAnchor:self.contentImageView.trailingAnchor constant:-10],
       
       
       // Publisher label
       [self.publisherLabel.topAnchor constraintEqualToAnchor:self.titleLabel.bottomAnchor constant:10],
       [self.publisherLabel.leadingAnchor constraintEqualToAnchor:self.titleLabel.leadingAnchor],
       [self.publisherLabel.bottomAnchor constraintEqualToAnchor:self.container.bottomAnchor constant:-10],
       
       // Separator Text
       [self.separatorLabel.leftAnchor constraintEqualToAnchor:self.publisherLabel.rightAnchor],
       [self.separatorLabel.bottomAnchor constraintEqualToAnchor:self.publisherLabel.bottomAnchor],
       
       // Time label
       [self.timeLabel.leftAnchor constraintEqualToAnchor:self.separatorLabel.rightAnchor],
       [self.timeLabel.bottomAnchor constraintEqualToAnchor:self.separatorLabel.bottomAnchor],
       ]];
}


- (void)configureCellWith:(Content *)content {
    [self.contentImageView sd_setImageWithURL:content.avatarUrl];
    self.titleLabel.text = content.title;
    self.publisherLabel.text = content.publisherName;
    
    NSDateComponents *dateComponent = [Utility dateComponentsFromDate:content.date toDate:[NSDate date]];
    self.timeLabel.text = [ContentTableViewCell timeStringFormatted:dateComponent];
}

@end
