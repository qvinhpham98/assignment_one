//
//  BaseTableViewCell.h
//  AssignmentOne
//
//  Created by LAP12230 on 2/13/20.
//  Copyright © 2020 LAP12230. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BaseTableViewCell: UITableViewCell

- (void)setupSubviews;
- (void)setupConstraints;

+ (NSString *) reuseIdentifier;

@end

NS_ASSUME_NONNULL_END
