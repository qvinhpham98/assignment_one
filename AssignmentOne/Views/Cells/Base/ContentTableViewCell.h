//
//  BaseNewsTableViewCell.h
//  AssignmentOne
//
//  Created by LAP12230 on 2/13/20.
//  Copyright © 2020 LAP12230. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseTableViewCell.h"
#import "Content.h"

NS_ASSUME_NONNULL_BEGIN

@interface ContentTableViewCell : BaseTableViewCell

- (void)configureCellWith:(Content *)content;

+ (NSString *) getCellReuseIdentifierForContent:(Content *)content atIndex:(NSInteger)index;
+ (NSString *) timeStringFormatted:(NSDateComponents *)dateComponents;

@end

NS_ASSUME_NONNULL_END
