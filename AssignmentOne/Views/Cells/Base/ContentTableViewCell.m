//
//  BaseNewsTableViewCell.m
//  AssignmentOne
//
//  Created by LAP12230 on 2/13/20.
//  Copyright © 2020 LAP12230. All rights reserved.
//

#import "ContentTableViewCell.h"
#import "ContentTableViewCellTypeOne.h"
#import "ContentTableViewCellTypeTwo.h"
#import "ContentTableViewCellTypeThree.h"

@implementation ContentTableViewCell

- (void)configureCellWith:(Content *)content {
    // Empty
}


+ (NSString *) getCellReuseIdentifierForContent:(Content *)content atIndex:(NSInteger)index {
    NSString *returnValue;
    
    if ((index + 1) % 5 == 0) {
        returnValue = ContentTableViewCellTypeThree.reuseIdentifier;
    } else if (content.images.count > 2) {
        returnValue = ContentTableViewCellTypeTwo.reuseIdentifier;
    } else {
        returnValue = ContentTableViewCellTypeOne.reuseIdentifier;
    }
    
    return returnValue;
}


+ (NSString *)timeStringFormatted:(NSDateComponents *)dateComponents {
    NSString *returnValue = @"1 phút";
    if (dateComponents.hour != 0) {
        returnValue = [NSString stringWithFormat:@"%ld giờ", (long)dateComponents.hour];
    } else if (dateComponents.minute != 0) {
        returnValue = [NSString stringWithFormat:@"%ld phút", (long)dateComponents.minute];
    }
    
    return returnValue;
}

@end
