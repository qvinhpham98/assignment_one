//
//  NewsViewController.m
//  AssignmentOne
//
//  Created by LAP12230 on 2/13/20.
//  Copyright © 2020 LAP12230. All rights reserved.
//

#import "NewsViewController.h"
#import "ContentTableViewCell.h"
#import "SectionBox.h"
#import "ContentTableViewCellTypeOne.h"
#import "ContentTableViewCellTypeTwo.h"
#import "ContentTableViewCellTypeThree.h"

#define DATA_URL @"https://data.baomoi.com/sectionbox/get?zone=c_0&imgsize=a500x&apikey=ee7b8c7c7019f1b5c0674d41b125faf7&ctime=1537957745&sig=69f56506bfead1f0c90a820a4a76f058"

@interface NewsViewController ()

@property NSMutableArray<SectionBox *> *sectionBoxes;

- (void)setupTableView;
- (void)fetchData;

@end

@implementation NewsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupTableView];
    [self fetchData];
}


- (void)setupTableView {
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    [self.tableView registerClass:ContentTableViewCellTypeOne.class forCellReuseIdentifier:ContentTableViewCellTypeOne.reuseIdentifier];
    [self.tableView registerClass:ContentTableViewCellTypeTwo.class forCellReuseIdentifier:ContentTableViewCellTypeTwo.reuseIdentifier];
    [self.tableView registerClass:ContentTableViewCellTypeThree.class forCellReuseIdentifier:ContentTableViewCellTypeThree.reuseIdentifier];
}


- (void)fetchData {
    NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:DATA_URL] options:kNilOptions error:NULL];
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:NULL];
    NSArray *sectionBoxesJson = dict[@"data"];
    
    self.sectionBoxes = [[NSMutableArray<SectionBox *> alloc] init];
    for (NSDictionary *box in sectionBoxesJson) {
        SectionBox *sectionBox = [[SectionBox alloc] initWithDictionary:box error:NULL];
        if (sectionBox != NULL) {
            [self.sectionBoxes addObject:sectionBox];
        }
    }
}


#pragma - UITableView DataSources & Delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.sectionBoxes.count;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.sectionBoxes[section].contents.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    Content *content = self.sectionBoxes[indexPath.section].contents[indexPath.row];
    NSString *reuseIdentifier = [ContentTableViewCell getCellReuseIdentifierForContent:content atIndex:indexPath.row];
    
    ContentTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    [cell configureCellWith:content];
    return cell;
}


@end
