//
//  File.h
//  AssignmentOne
//
//  Created by LAP12230 on 2/18/20.
//  Copyright © 2020 LAP12230. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface FileUtility : NSObject

+ (NSString *) fileNameAtPath:(NSString *)path;
+ (BOOL) renameFileAtPath:(NSString *)path withName:(NSString *)newName;

+ (NSArray<NSString *> *) truncateFileAtPath:(NSString *)path toPath:(NSString *)toPath maxiumChunkSizeInMb:(float)mbChunkSize;
+ (void)mergeFilesAtFolder:(NSString *)path destinationPath:(NSString *)dPath;
+ (void)mergeFilesAtPaths:(NSArray<NSString *> *)paths destinationPath:(NSString *)dPath;
+ (void) processedDuplicateFiles:(NSArray<NSString *> *)filePaths;
+(NSArray<NSString *> *) processedDuplicateFileNames:(NSArray<NSString *> *)fileNames;

@end

NS_ASSUME_NONNULL_END
