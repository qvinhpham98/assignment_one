//
//  File.m
//  AssignmentOne
//
//  Created by LAP12230 on 2/21/20.
//  Copyright © 2020 LAP12230. All rights reserved.
//

#import "File.h"

@implementation File

- (instancetype)initWithPath:(NSString *)path {
    self = [super init];
    if (self) {
        
        NSFileManager *fileManager = [NSFileManager defaultManager];
        BOOL isDirectory;

        // Check if the path is valid
        if (![fileManager fileExistsAtPath:path isDirectory:&isDirectory]) {
            
            // If a file is not existed -> Continue check if the folder of it is existed
            if ([fileManager fileExistsAtPath:[path stringByDeletingLastPathComponent]]) {
                
                // The folder is existed but the file is not => Create file
                [fileManager createFileAtPath:path contents:nil attributes:nil];
                isDirectory = false;
                
            } else {
                
                // Both file and folder is not exist
                // => Path is invalid
                return nil;
            }
            
        }
        
        _path = path;
        _name = [path lastPathComponent];
        _url = [NSURL URLWithString:path];
        _extension = [_name pathExtension];
        _isDirectory = isDirectory;
        _defaultFileManager = fileManager;
    }
    
    return self;
}


- (instancetype)initWithURL:(NSURL *)url {
    return [self initWithPath:[url absoluteString]];
}


- (BOOL)renameFileWithName:(NSString *)newName retainedOldExtension:(BOOL)retainedOldExtension {
    
    // Validate file name
    if (!newName) return false;
    
    // File name
    NSString *fileName = newName;
    if (retainedOldExtension) {
        fileName = [fileName stringByAppendingPathExtension:[self.name pathExtension]];
    }
    
    // New path
    NSString *dir = [self.path stringByDeletingLastPathComponent];
    NSString *newPath = [dir stringByAppendingPathComponent:fileName];
    
    BOOL isSuccess = [self.defaultFileManager moveItemAtPath:self.path toPath:newPath error:nil];
    
    // Update info
    [self updateFileInfoWithPath:newPath];
    return isSuccess;
}


- (BOOL)moveFileToPath:(NSString *)path {
    if (self.isDirectory) return false;
    
    BOOL isSuccess = [self.defaultFileManager moveItemAtPath:self.path toPath:path error:nil];
    if (isSuccess) {
        [self updateFileInfoWithPath:path];
    }
    
    return isSuccess;
}


- (BOOL)moveFileToURL:(NSURL *)url {
    return [self moveFileToPath:[url absoluteString]];
}


- (BOOL)copyFileToPath:(NSString *)path {
    if (self.isDirectory) return false;
    return [self.defaultFileManager copyItemAtPath:self.path toPath:path error:nil];
}


- (BOOL)copyFileToURL:(NSURL *)url {
    return [self copyFileToPath:[url absoluteString]];
}


- (void)performAction:(FileAction)action blockAction:(void (^)(NSFileHandle * _Nonnull))blockAction {
    NSFileHandle *fileHandle;
    switch (action) {
        case FileActionReading:
            fileHandle = [NSFileHandle fileHandleForReadingAtPath:self.path];
            break;
            
        case FileActionWriting:
            fileHandle = [NSFileHandle fileHandleForWritingAtPath:self.path];
            break;
    }
    
    blockAction(fileHandle);
    [fileHandle closeFile];
}


- (void) loadImageFromPath:(NSString *)path completionBlock:(void (^)(UIImage * _Nullable))completionBlock {
    
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:path]];
        UIImage *image = [UIImage imageWithData:imageData];
        
        if (image) {
            [self.defaultFileManager createFileAtPath:self.path contents:imageData attributes:nil];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (completionBlock) {
                completionBlock(image);
            }
        });
    });
    
}


- (void)enumerateChild:(void (^)(File * _Nonnull))actionBlock {
    
    if (!self.isDirectory) return;
    
    NSArray<NSString *> *subPaths = [self.defaultFileManager subpathsAtPath:self.path];
    for (NSString *path in subPaths) {
        File *childFile = [[File alloc] initWithPath:path];
        actionBlock(childFile);
    }
}

#pragma - PRIVATE METHODS

- (void)updateFileInfoWithPath:(NSString *)path {
    BOOL isDirectory;
    [self.defaultFileManager fileExistsAtPath:path isDirectory:&isDirectory];
    
    _path = path;
    _name = [path lastPathComponent];
    _url = [NSURL URLWithString:path];
    _extension = [_name pathExtension];
    _isDirectory = isDirectory;
}


- (void)updateFileInfoWithURL:(NSURL *)url {
    [self updateFileInfoWithPath:[url absoluteString]];
}

@end
