//
//  Constants.h
//  AssignmentOne
//
//  Created by LAP12230 on 2/14/20.
//  Copyright © 2020 LAP12230. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Constants : NSObject

extern double const CONTENT_PADDING;
extern double const CONTENT_IMAGE_HEIGHT;
extern double const CONTENT_IMAGE_BORDER_RADIUS;
extern double const CONTENT_TITLE_SIZE;

@end

NS_ASSUME_NONNULL_END
