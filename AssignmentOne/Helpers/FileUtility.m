//
//  File.m
//  AssignmentOne
//
//  Created by LAP12230 on 2/18/20.
//  Copyright © 2020 LAP12230. All rights reserved.
//

#import "FileUtility.h"

@implementation FileUtility

+ (NSArray<NSString *> *) truncateFileAtPath:(NSString *)path toPath:(NSString *)toPath maxiumChunkSizeInMb:(float)mbChunkSize {
    
    // Validate path
    NSFileHandle *readingFileHandle = [NSFileHandle fileHandleForReadingAtPath:path];
    if (!readingFileHandle) return nil;
    
    // Declaring
    NSFileManager *fileManager = NSFileManager.defaultManager;
    NSMutableArray<NSString *> *resultArray = [[NSMutableArray alloc] init];
    NSUInteger fileOrder = 1;
    NSString *fileName = [path lastPathComponent];
    
    unsigned long long offset = 0;
    unsigned long long totalSize = [readingFileHandle seekToEndOfFile];
    unsigned long long chunkSize = mbChunkSize * 1000 * 1000;
    unsigned long readingChunkSize = 8000;                                         // Small enough for optimizing ram while reading & writing.
    
    NSData *tempData;
    while (offset < totalSize) {
        
        // Create new writing file chunk
        NSString *filePath;
        filePath = [toPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@%lu.data", fileName, (unsigned long)fileOrder]];
        [fileManager createFileAtPath:filePath contents:nil attributes:nil];
        fileOrder += 1;
        [resultArray addObject:filePath];
        
        NSFileHandle *writingFileHandle = [NSFileHandle fileHandleForWritingAtPath:filePath];
        
        // Loop reading and writing with small chunk size per time
        // => Avoid Ram, CPU overloading.
        while ([writingFileHandle seekToEndOfFile] < chunkSize && offset < totalSize) {
            @autoreleasepool {
                [readingFileHandle seekToFileOffset:offset];
                tempData = [readingFileHandle readDataOfLength:readingChunkSize];
                [writingFileHandle writeData:tempData];
                offset += [tempData length];
            }
        }
        
        [writingFileHandle closeFile];
    }
    
    [readingFileHandle closeFile];
    return resultArray;
}


+ (void)mergeFilesAtPaths:(NSArray<NSString *> *)paths destinationPath:(NSString *)dPath {
    
    // Create empty result file
    NSFileManager *fileManager = NSFileManager.defaultManager;
    [fileManager createFileAtPath:dPath contents:nil attributes:nil];
    
    // Get handler
    NSFileHandle *outputFileHandle = [NSFileHandle fileHandleForWritingAtPath:dPath];
    
    // Merging
    for (NSString *path in paths) {
        if ([fileManager fileExistsAtPath:path]) {
            
            NSFileHandle *readingFileHandle = [NSFileHandle fileHandleForReadingAtPath:path];
            
            unsigned long long offset = 0;
            unsigned long long totalSize = [readingFileHandle seekToEndOfFile];
            unsigned long readingChunkSize = 8000;                              // Small enough for optimizing ram while reading & writing.
            
            while (offset < totalSize) {
                @autoreleasepool {
                    [readingFileHandle seekToFileOffset:offset];
                    NSData *tempData = [readingFileHandle readDataOfLength:readingChunkSize];
                    offset += tempData.length;
                    [outputFileHandle writeData:tempData];
                }
            }
            
            [readingFileHandle closeFile];
        }
    }
    
    [outputFileHandle closeFile];
}


+ (NSString *)fileNameAtPath:(NSString *)path {
    NSString *returnValue = @"";
    if ([[NSFileManager defaultManager] fileExistsAtPath:path]) {
        returnValue = [path lastPathComponent];
    }
    
    return returnValue;
}


+ (BOOL)renameFileAtPath:(NSString *)path withName:(NSString *)newName {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![[NSFileManager defaultManager] fileExistsAtPath:path]) return false;
    
    NSString *ext = [[FileUtility fileNameAtPath:path] pathExtension];
    NSString *newNameWithExt = [newName stringByAppendingPathExtension:ext];
    NSString *newPath = [[path stringByDeletingLastPathComponent] stringByAppendingPathComponent:newNameWithExt];
    return [fileManager moveItemAtPath:path toPath:newPath error:nil];
}


+ (void)mergeFilesAtFolder:(NSString *)path destinationPath:(NSString *)dPath {
    NSFileManager *fileManager = NSFileManager.defaultManager;
    NSArray *itemPaths = [fileManager contentsOfDirectoryAtPath:path error:nil];
    NSMutableArray *paths = [[NSMutableArray alloc] init];
    
    // Get full paths
    for (NSString *item in itemPaths) {
        if ([[item pathExtension]  isEqual: @"data"]) {
            NSString *fullItemPath = [path stringByAppendingPathComponent:item];
            [paths addObject:fullItemPath];
        }
    }
    
    [FileUtility mergeFilesAtPaths:paths destinationPath:dPath];
}


+ (void) processedDuplicateFiles:(NSArray<NSString *> *)filePaths {
    NSMutableDictionary<NSString *, NSNumber *> *dictNames = [[NSMutableDictionary alloc] init];
    for (NSString *path in filePaths) {
        NSString *fileName = [FileUtility fileNameAtPath:path];
        if (dictNames[fileName]) {
            NSString *nameWithoutExt = [fileName stringByDeletingPathExtension];
            NSInteger numb = [dictNames[fileName] integerValue];
            [FileUtility renameFileAtPath:path withName:[NSString stringWithFormat:@"%@(%ld)", nameWithoutExt, (long)numb]];
            dictNames[fileName] = [NSNumber numberWithInteger:[dictNames[fileName] integerValue] + 1];
        } else {
            dictNames[fileName] = [NSNumber numberWithInt:1];
        }
    }
}


+ (NSArray<NSString *> *) processedDuplicateFileNames:(NSArray<NSString *> *)fileNames {
    NSMutableDictionary<NSString *, NSNumber *> *dictNames = [[NSMutableDictionary alloc] init];
    NSMutableArray<NSString *> *output = [[NSMutableArray alloc] init];
    
    for (NSString *name in fileNames) {
        
        NSString *outputName = [NSString stringWithString:name];
        while (dictNames[outputName]) {
            outputName = [NSString stringWithFormat:@"%@(%@)", name, dictNames[name]];
            dictNames[name] = [NSNumber numberWithInteger:dictNames[name].integerValue + 1];
        }
        
        dictNames[outputName] = [NSNumber numberWithInteger:1];
        [output addObject:outputName];
    }
    
    return output;
}

@end
