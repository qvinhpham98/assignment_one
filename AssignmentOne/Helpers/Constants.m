//
//  Constants.m
//  AssignmentOne
//
//  Created by LAP12230 on 2/14/20.
//  Copyright © 2020 LAP12230. All rights reserved.
//

#import "Constants.h"

@implementation Constants

double const CONTENT_PADDING = 15;
double const CONTENT_IMAGE_HEIGHT = 85;
double const CONTENT_IMAGE_BORDER_RADIUS = 5.0;
double const CONTENT_TITLE_SIZE = 17;

@end
