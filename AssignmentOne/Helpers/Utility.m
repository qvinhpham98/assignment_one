//
//  Utilities.m
//  AssignmentOne
//
//  Created by LAP12230 on 2/17/20.
//  Copyright © 2020 LAP12230. All rights reserved.
//

#import "Utility.h"

@interface Utility ()



@end

@implementation Utility

+ (NSDateComponents *)dateComponentsFromDate:(NSDate *)fromDate toDate:(NSDate *)toDate {
    return [[NSCalendar currentCalendar] components:(NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute) fromDate:fromDate toDate:toDate options:0];
}

@end
