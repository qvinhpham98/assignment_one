//
//  Utilities.h
//  AssignmentOne
//
//  Created by LAP12230 on 2/17/20.
//  Copyright © 2020 LAP12230. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Utility : NSObject

+ (NSDateComponents *) dateComponentsFromDate:(NSDate *)fromDate toDate:(NSDate *)toDate;

@end

NS_ASSUME_NONNULL_END
