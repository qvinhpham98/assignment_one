//
//  ContentImage.h
//  AssignmentOne
//
//  Created by LAP12230 on 2/14/20.
//  Copyright © 2020 LAP12230. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <JSONModel.h>

NS_ASSUME_NONNULL_BEGIN

@protocol ContentImage;
@interface ContentImage : NSObject

@property (nonatomic) NSString *urlString;
@property (nonatomic) NSInteger *width;
@property (nonatomic) NSInteger *height;
@property (nonatomic, getter=url) NSURL <Ignore> *url;

- (NSURL *)url;

@end

NS_ASSUME_NONNULL_END
