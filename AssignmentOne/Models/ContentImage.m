//
//  ContentImage.m
//  AssignmentOne
//
//  Created by LAP12230 on 2/14/20.
//  Copyright © 2020 LAP12230. All rights reserved.
//

#import "ContentImage.h"

@implementation ContentImage


+ (JSONKeyMapper *)keyMapper {
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:
            @{
              @"urlString": @"url",
              @"width": @"width",
              @"height": @"height",
              }];
}


- (NSURL *)url {
    return [NSURL URLWithString:self.urlString];
}


@end
