//
//  SectionBox.h
//  AssignmentOne
//
//  Created by LAP12230 on 2/13/20.
//  Copyright © 2020 LAP12230. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <JSONModel.h>
#import "Content.h"

NS_ASSUME_NONNULL_BEGIN

@interface SectionBox : JSONModel

@property (nonatomic) NSInteger id;
@property (nonatomic) NSString *title;
@property (nonatomic) NSInteger maxShow;
@property (nonatomic) NSInteger displayType;
@property (nonatomic) NSArray <Content *> <Content> *contents;

@end

NS_ASSUME_NONNULL_END
