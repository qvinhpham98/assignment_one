//
//  News.h
//  AssignmentOne
//
//  Created by LAP12230 on 2/13/20.
//  Copyright © 2020 LAP12230. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <JSONModel.h>
#import "ContentImage.h"

NS_ASSUME_NONNULL_BEGIN

@protocol Content;
@interface Content : JSONModel

@property (nonatomic) NSInteger id;
@property (nonatomic) NSInteger contentType;
@property (nonatomic) NSString *title;
@property (nonatomic) NSString *avatarUrlString;
@property (nonatomic) NSString *publisherName;
@property (nonatomic) double timeIntervalSince1970;
@property (nonatomic) NSArray <ContentImage *> <ContentImage> *images;
@property (nonatomic) NSURL <Ignore> *avatarUrl;
@property (nonatomic) NSDate <Ignore> *date;

@end

NS_ASSUME_NONNULL_END
