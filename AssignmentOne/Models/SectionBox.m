//
//  SectionBox.m
//  AssignmentOne
//
//  Created by LAP12230 on 2/13/20.
//  Copyright © 2020 LAP12230. All rights reserved.
//

#import "SectionBox.h"

@implementation SectionBox

+ (JSONKeyMapper *)keyMapper {
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:
            @{
              @"id": @"sectionBoxId",
              @"title": @"title",
              @"maxShow": @"maxShow",
              @"displayType": @"displayType",
              @"contents": @"items"
              }];
}

@end
