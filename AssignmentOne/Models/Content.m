//
//  News.m
//  AssignmentOne
//
//  Created by LAP12230 on 2/13/20.
//  Copyright © 2020 LAP12230. All rights reserved.
//

#import "Content.h"

@implementation Content

+ (JSONKeyMapper *)keyMapper {
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:
            @{
              @"id": @"contentId",
              @"title": @"title",
              @"avatarUrlString": @"avatarUrl",
              @"contentType": @"contentTypes",
              @"publisher": @"publisherName",
              @"timeIntervalSince1970": @"date",
              @"images": @"images",
              }];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict error:(NSError *__autoreleasing *)err {
    self = [super initWithDictionary:dict error:err];
    if (self) {
        self.date = [NSDate dateWithTimeIntervalSince1970:self.timeIntervalSince1970];
        self.avatarUrl = [NSURL URLWithString:self.avatarUrlString];
    }
    return self;
}

- (NSURL *)avatarUrl {
    return [NSURL URLWithString:self.avatarUrlString];
}

@end
